terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.region
}

data "aws_caller_identity" "current" {}

variable "env" {
  type    = string
  default = "poc"
}

variable "region" {
  type    = string
  default = "eu-central-1"
}

variable "project" {
  type    = string
  default = "example"
}

locals {
  tags = {
    Terraform   = "true"
    Environment = var.env
    Project     = var.project
  }
}