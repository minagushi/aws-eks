terraform {
  backend "s3" {
    bucket = "demo-non-prod-terraform-states"
    key    = "poc/vpc"
    region = "eu-central-1"
  }
}