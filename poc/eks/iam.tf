resource "aws_iam_role" "service_account_role" {
  name = format("%s-%s-eks-pod-example", var.env, var.project)

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Principal" : {
          "Federated" : "${local.oidc_provider_arn}"
        },
        "Action" : "sts:AssumeRoleWithWebIdentity",
        "Condition" : {
          "StringEquals" : {
            "${local.cluster_oidc_issuer}:sub" : "system:serviceaccount:test-namespace:test-service-account"
          }
        }
      }
    ]
  })

  tags = local.tags
}

resource "aws_iam_policy" "service_account_policy" {
  name        = format("%s-%s-eks-pod-s3", var.env, var.project)
  path        = "/"
  description = "Policy for test K8S service_account"

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "s3:PutObject",
          "s3:GetObject",
          "s3:ListBucket",
          "s3:ListAllMyBuckets",
          "s3:DeleteObject"
        ],
        "Resource" : [
          "arn:aws:s3:::${aws_s3_bucket.service_account_access_test.id}/*",
          "arn:aws:s3:::${aws_s3_bucket.service_account_access_test.id}",
        ]
      }
    ]
  })
  tags = local.tags
}

resource "aws_iam_policy_attachment" "test-attach" {
  name       = format("%s-%s-eks-pod-s3", var.env, var.project)
  roles      = [aws_iam_role.service_account_role.name]
  policy_arn = aws_iam_policy.service_account_policy.arn
}