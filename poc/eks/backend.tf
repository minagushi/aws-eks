terraform {
  backend "s3" {
    bucket = "demo-non-prod-terraform-states"
    key    = "poc/eks"
    region = "eu-central-1"
  }
}