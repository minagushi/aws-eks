data "aws_eks_cluster_auth" "default" {
  name = module.eks.cluster_id
}

provider "kubernetes" {
  host                   = module.eks.cluster_endpoint
  cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
  token                  = data.aws_eks_cluster_auth.default.token
}

resource "kubernetes_namespace" "test" {
  metadata {
    name = "test-namespace"
  }
}

resource "kubernetes_service_account" "example" {
  metadata {
    name        = "test-service-account"
    namespace   = kubernetes_namespace.test.metadata[0].name
    annotations = {
      "eks.amazonaws.com/role-arn" = aws_iam_role.service_account_role.arn
    }
  }
}

resource "kubernetes_pod" "test" {
  count = var.depoy_test_pod ? 1 : 0

  metadata {
    name      = "terraform-example"
    namespace = kubernetes_namespace.test.metadata[0].name
  }

  spec {
    container {
      image = "amazon/aws-cli"
      name  = "awscli"
      command = [
        "sleep",
        "604800"
      ]
    }
    service_account_name = kubernetes_service_account.example.metadata[0].name
  }
}