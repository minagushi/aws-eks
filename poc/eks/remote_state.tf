
data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = "demo-non-prod-terraform-states"
    key    = format("%s/vpc", var.env)
    region = var.region
  }
}