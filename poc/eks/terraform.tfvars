cluster_endpoint_private_access = true
cluster_endpoint_public_access  = true
enable_irsa                     = true
openid_connect_audiences        = ["sts.amazonaws.com"]
cluster_addons = {
  coredns = {
    resolve_conflicts = "OVERWRITE"
  }
  kube-proxy = {}
  vpc-cni = {
    resolve_conflicts = "OVERWRITE"
  }
}
eks_managed_node_group_defaults = {
  ami_type       = "AL2_x86_64"
  disk_size      = 50
  instance_types = ["t3.large"]
}
eks_managed_node_groups = {
  main = {
    min_size     = 1
    max_size     = 2
    desired_size = 1

    instance_types = ["t3.large"]
    capacity_type  = "SPOT"
  }
}
depoy_test_pod = false