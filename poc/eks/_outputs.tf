output "eks" {
  value = module.eks
}

output "test_service_account_name" {
  value = kubernetes_service_account.example.metadata[0].name
}