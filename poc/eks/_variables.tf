variable "cluster_endpoint_private_access" {}
variable "cluster_endpoint_public_access" {}
variable "enable_irsa" {}
variable "openid_connect_audiences" {}
variable "cluster_addons" {}
variable "eks_managed_node_group_defaults" {}
variable "eks_managed_node_groups" {}
variable "cluster_version" {
  default = null
}
variable "depoy_test_pod" {}