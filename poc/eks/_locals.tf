locals {
  private_subnets         = data.terraform_remote_state.vpc.outputs.vpc.private_subnets
  vpc_id                  = data.terraform_remote_state.vpc.outputs.vpc.vpc_id
  cluster_name            = format("%s-%s-main", var.env, var.project)
  cluster_oidc_issuer = trim(module.eks.cluster_oidc_issuer_url, "https://")
  oidc_provider_arn       = module.eks.oidc_provider_arn
  account_id              = data.aws_caller_identity.current.account_id
}