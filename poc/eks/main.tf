module "eks" {
  source = "terraform-aws-modules/eks/aws"

  vpc_id     = local.vpc_id
  subnet_ids = local.private_subnets

  cluster_name                    = local.cluster_name
  cluster_version                 = var.cluster_version
  cluster_endpoint_private_access = var.cluster_endpoint_private_access
  cluster_endpoint_public_access  = var.cluster_endpoint_public_access
  cluster_addons                  = var.cluster_addons

  enable_irsa              = var.enable_irsa
  openid_connect_audiences = var.openid_connect_audiences

  eks_managed_node_group_defaults = var.eks_managed_node_group_defaults
  eks_managed_node_groups         = var.eks_managed_node_groups

  tags = local.tags
}

resource "aws_s3_bucket" "service_account_access_test" {
  bucket = format("%s-%s-svcaccount-access-test", var.env, var.project)
  acl    = "private"
  tags   = local.tags
}