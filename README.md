# Technical Task (Automate EKS cluster setup on AWS)

## introduction

This repo contains example terraform code which deploys following components

- VPC With private and public subnets (Used public module: https://github.com/terraform-aws-modules/terraform-aws-vpc )
- EKS Cluster with main node group (Used public module: https://github.com/terraform-aws-modules/terraform-aws-eks)
- Some example k8s resources for IAM OIDC provider

## Prerequisites

- Terraform installed ( https://learn.hashicorp.com/tutorials/terraform/install-cli )
- kubectl installed ( https://kubernetes.io/docs/tasks/tools/ )
- awscli installed ( https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html )
- User with Administrator permissions for AWS Account ( https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html )
- Created terraform state bucket in AWS Account ( https://docs.aws.amazon.com/AmazonS3/latest/userguide/create-bucket-overview.html )

## Usage

First of all tweak general env variables in poc/env.tf such as aws region, project name etc.

### Deploy VPC

1. Run`cd poc/vpc`
2. Change values in `terraform.tfars` of your liking or leave with defaults.
3. Change bucket name in `backend.tf` to bucket you've created in Prerequisites.
4. Run `terraform init` to initialise.
5. Run `terraform apply`
6. Verify plan and resources will be provisioned as an intermediate output of `apply`.
7. Type `yes` and press Enter.

### Deploy EKS Cluster

1. Run `cd poc/eks`
2. Change values in `terraform.tfars` of your liking or leave with defaults.
3. Change bucket name in `backend.tf` and `remote_state.tf` to bucket you've created in Prerequisites.
4. Run `terraform init` to initialise.
5. Run `terraform apply`
6. Verify plan and resources will be provisioned as an intermediate output of `apply`.
7. Type `yes` and press Enter.
8. As an output you'll see all information about EKS Cluster provisioned and S3 bucket which will be accessed by example pod.

### How to use IAM Roles with k8s Service Accounts

As an additional prerequisite you'll need to create kubectl config for new cluste: Run `aws eks update-kubeconfig --region eu-central-1 --name <cluster_name_from_output of cluster deployment>`

In file `poc/eks/iam.tf` you can find example IAM resources defined to be assumed by service account entity in k8s cluster:
- aws_iam_policy.service_account_policy - policy which defines permission boundaries for service account's role
- aws_iam_role.service_account_role - role to be assumed

In file `poc/eks/k8s.tf` you can find example k8s resources defined to test service account:
- kubernetes_namespace.test - test-namespace
- kubernetes_service_account.example - example service account created in test-namespace
- kubernetes_pod.test - test pod with aws cli image

1. Run `cd poc/eks`
2. Run `terraform apply -var="depoy_test_pod=true"`
3. Run `kubectl -n test-namespace exec -it terraform-example -- /bin/bash`
4. You'll get to shell of running pod with example service account and you can run some commands like `aws s3 ls s3://poc-example-svcaccount-access-test` to test behaviour or just run `aws sts get-caller-identity` to see information about asumed role.

# Research Task (Managing secrets on Kubernetes)

One option is implement AWS Secrets Manager/Parameter Store to manage secrets and inject them in k8s cluster in mounts and configure application to lookup for them. During CI\CD process k8s manifests\helm secret names will be updated with respective value to environment.
(https://docs.aws.amazon.com/secretsmanager/latest/userguide/integrating_csi_driver.html)

Second option is a cheaper version of solution is to use CI(eg Jenkins\Gitlab) to store secrets. During depoyment create k8s secrets or update configs directly in CI process.